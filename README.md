# OpenML dataset: Diabetes_Dataset

https://www.openml.org/d/46254

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The dataset, named 'diabetes.csv', serves as a comprehensive resource for understanding various factors that may influence the occurrence of diabetes in individuals. Consisting of several medically relevant parameters, the dataset captures key details across 9 columns, namely Pregnancies, Glucose, BloodPressure, SkinThickness, Insulin, BMI (Body Mass Index), DiabetesPedigreeFunction, Age, and Outcome. Each column reflects a distinct attribute significant to diabetes research and potential predictive modeling.

Attribute Description:
1. Pregnancies: Number of times pregnant (Example values: 2, 1)
2. Glucose: Plasma glucose concentration over 2 hours in an oral glucose tolerance test (Example values: 82, 142)
3. BloodPressure: Diastolic blood pressure (mm Hg) (Example values: 70, 64)
4. SkinThickness: Triceps skin fold thickness (mm) (Example values: 27, 0)
5. Insulin: 2-Hour serum insulin (mu U/ml) (Example values: 168, 0)
6. BMI: Body mass index (weight in kg/(height in m)^2) (Example values: 36.8, 30.1)
7. DiabetesPedigreeFunction: Diabetes pedigree function (Example values: 0.34, 0.396)
8. Age: Age in years (Example values: 54, 24)
9. Outcome: Class variable (0 or 1) where 1 denotes the presence of diabetes and 0 denotes absence (Example values: 1, 0)

Use Case:
This dataset is particularly useful for medical researchers, data scientists, and healthcare providers seeking to identify patterns or factors that significantly contribute to diabetes. By employing statistical analysis or machine learning models, one can predict the likelihood of diabetes occurrence based on the dataset's parameters. Furthermore, this dataset can facilitate a better understanding of how various factors, such as pregnancy, BMI, and age, interact with each other in the context of diabetes, thereby aiding in preventative healthcare planning and patient education.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46254) of an [OpenML dataset](https://www.openml.org/d/46254). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46254/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46254/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46254/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

